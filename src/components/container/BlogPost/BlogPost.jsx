import React, {Component, Fragment} from "react";
import './BlogPost.css';

class BlogPost extends Component {
    render(){
        return(
            <Fragment>
            <p className="section-title">Blog Post</p>
            <div className="post">
                <div className="img-thumb">
                    <img src="http://placeing,com/200/154/tech" alt="dummy"/>
            </div>
            <div className="content">
                <p className="tittle">Dummy Title</p>
                <p className="desc" >Dummy body here</p>
            </div>
            </div>
            </Fragment>
        )
    }
}
export default BlogPost;