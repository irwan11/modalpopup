import React, {com} from 'react';
import './Modal.css';

const modal = (props) => {
    console.log('Result', props)
    return (
        <div>
            <div className="modal-wrapper"
                style={{
                    transform: props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}>
                    
                <div className="modal-body">
                    <p>
                    {String(props.result)}
                    </p>
                </div>
                
                <div className="modal-footer">
                <p>
                    {props.result}
                    </p>
                    
                    
                    <button className="btn-cancel" onClick={props.close}>CLOSE</button>
                    <button className="btn-continue">CONTINUE</button>
                </div>
            </div>
        </div>
    )
}

export default modal;
