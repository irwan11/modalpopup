import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Home from './components/container/home/Home';
import BlogPost from'./components/container/BlogPost/BlogPost';

ReactDOM.render(<App />, document.getElementById('root'));
