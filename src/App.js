import React, { Component } from 'react';

import Modal from './components/Modal/Modal';


class App extends Component {
	 palindrom() {
        var origString;
        var InputStr = document.getElementById('tbox').value; //
        //
        InputStr = InputStr.toLowerCase();
        //
        origString= InputStr;
        //
        InputStr = InputStr.split(''); 
        InputStr = InputStr.reverse(); 
        InputStr = InputStr.join(''); 
        var revString = InputStr;
        var resultPalindrom;
        //
        if(origString === revString){
          resultPalindrom = true;
        }
        else
        {
          resultPalindrom = false;
        }
        
        return resultPalindrom;
      };
	constructor() {
		
		super();

		this.state = {
			isShowing: false,
			result: ''
		}
	}

	openModalHandler = () => {
		let result = this.palindrom();
		
		this.setState({
			isShowing: true,
			result: result,
		});
	}

	closeModalHandler = () => {
		this.setState({
			isShowing: false
		});
	}

	render () {
		return (
			<div>
				{ this.state.isShowing ? <div onClick={this.closeModalHandler} className="back-drop"></div> : null }
				
				
				   <button className="open-modal-btn" onClick={this.openModalHandler}>Click</button>
				 
				

				<Modal
					className="modal"
					show={this.state.isShowing}
					result={this.state.result}
					close={this.closeModalHandler}>
						
				</Modal>
				
				
				
        		<form action="" method="get"> 
        		<table>
       			<tr>
				   <td><h4>Palindrome</h4></td>
					   <td>
				   <input type="text" id="tbox" placeholder="masukkan" />
       			</td>
				   
				   </tr>
				</table>
				</form>
				
			</div>
			
				
			
		);
	}
}

export default App;
